alias be='bundle exec'
alias dc='docker-compose'
alias start='bundle exec rails s -b 0.0.0.0'
alias review='bundle exec rake db:migrate:reset RAILS_ENV=test'
alias migrate='bundle exec rake db:migrate'
alias noti="terminal-notifier -message 'コマンド完了'"
alias relogin='exec $SHELL -l'

alias st='git status'
alias fe='git fetch'