set -g theme_color_scheme dark

alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -la'

alias be='bundle exec'
alias dc='docker-compose'
alias start='bundle exec rails s -b 0.0.0.0'
alias review='bundle exec rake db:migrate:reset RAILS_ENV=test'
alias migrate='bundle exec rake db:migrate'
alias noti="terminal-notifier -message 'コマンド完了'"
alias relogin='source ~/.config/fish/config.fish'
alias st='git status'
alias fe='git fetch'

#peco
function fish_user_key_bindings
    bind \cr peco_select_history
end

status --is-interactive; and source (rbenv init -|psub)

