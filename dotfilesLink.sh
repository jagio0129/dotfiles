#! /bin/bash
git submodule init
git submodule update

### link
ln -s ~/dotfiles/.vimrc ~/.vimrc
ln -s ~/dotfiles/.vim ~/.vim
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf


mv ~/.bash_profile ~/.bash_profile.bu
ln -s ~/dotfiles/.bash_profile ~/.bash_profile

mv ~/.bashrc ~/.bashrc.bu
ln -s ~/dotfiles/.bashrc ~/.bashrc

mv ~/.bash_aliases ~/.bash_aliases.bu
ln -s ~/dotfiles/.bash_aliases ~/.bash_aliases

mv ~/.config/fish/config.fish ~/.config/fish/config.fish.bu
ln -s ~/dotfiles/config.fish ~/.config/fish/config.fish

cat .gitconfig >> ~/.gitconfig

# for mac
if [ "$(uname)" == 'Darwin' ]; then
  # terminal-notifier
  TERMINAL_NOTIFIER=`brew list | grep terminal-notifier`
  if [ ! $TERMINAL_NOTIFIER == 'terminal-notifier' ]; then
    brew install terminal-notifier;
  fi

  # tmux
  TMUX=`brew list | grep tmux`
  if [ ! $TERMINAL_NOTIFIER == 'tmux' ]; then
    brew install tmux
  fi
fi

# show git branch in prompt
curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh >> ~/.git-prompt.sh
source ~/.git-prompt.sh

exec $SHELL -l
